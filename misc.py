#code with misc functions needed for the project
import json
import os
from tqdm import tqdm
import nltk, re, pprint
from nltk import word_tokenize
from nltk.tokenize import TweetTokenizer
from nltk.corpus import stopwords 
from nltk.stem import PorterStemmer
from nltk.collocations import *
import string
import re
from nltk.tokenize import word_tokenize
from bs4 import BeautifulSoup
from urllib import request
import sys
import io
import ast

stopwords_english = stopwords.words('english')
stemmer = PorterStemmer()
emoticons = set([
    ':-\)', ':\)', ';\)', ':o\)', ':\]', ':3', ':c\)', ':>', '=\]', '8\)', '=\)', ':\}',
    ':^\)', ':-D', ':D', '8-D', '8D', 'x-D', 'xD', 'X-D', 'XD', '=-D', '=D',
    '=-3', '=3', ':-\)\)', ":'-\)", ":'\)", '>:P', ':-P', ':P', 'X-P',
    'x-p', 'xp', 'XP', ':-p', ':p', '=p', ':-b', ':b', '>:\)', '>;\)', '>:-\)',
    '<3' , ':L', ':-/', '>:/', ':S', '>:\[', ':@', ':-\(', ':\[', ':-||', '=L', ':<',
    ':-\[', ':-<', '=/', '>:\(', ':\(', '>.<', ":'-\(", ":'\(", ':-c',
    ':c', ':\{', ';\(', '…', '“', '”'
    ])

emoji_pattern = re.compile("["
        u"\U0001F600-\U0001F64F"  # emoticons
        u"\U0001F300-\U0001F5FF"  # symbols & pictographs
        u"\U0001F680-\U0001F6FF"  # transport & map symbols
        u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                           "]+", flags=re.UNICODE)

def fetch_tweet(tweetid):
	tweetid =str(tweetid)
	base = "https://twitter.com/anyuser/status/"
	url = base+tweetid
	try:
		html = request.urlopen(url).read().decode('utf8')
		raw = BeautifulSoup(html).get_text()

		start = 'Twitterissä: "'
		end = '"\n'
		st = raw.find(start)
		en = raw.find(end)

		if st == -1 or en == -1:
			return None
	
		tweet = raw[st+len(start):en]

		return tweet
	except:
		return None	

def tokenize_and_clean_tweets(tweet):
	""" code from :http://blog.chapagain.com.np/python-nltk-twitter-sentiment-analysis-natural-language-processing-nlp/
		input: a raw tweet
		output: stemmed cleaned tokens from tweet 
	"""

	# remove emoticons
	
	# remove stock market tickers like $GE
	tweet = re.sub(r'\$\w*', '', tweet)

	# remove old style retweet text "RT"
	tweet = re.sub(r'^RT[\s]+', '', tweet)

	# remove hyperlinks
	tweet = re.sub(r'https?:\/\/.*[\r\n]*', '', tweet)

	# remove hashtags
	# only removing the hash # sign from the word
	tweet = re.sub(r'#', '', tweet)

	# remove @ sign
	# only removing the at @ sign from the word
	tweet = re.sub(r'@', '', tweet)

	# remove emojis
	tweet = emoji_pattern.sub(r'', tweet)

	#tokenize tweets
	tknzr = TweetTokenizer(preserve_case=False, reduce_len=True)
	twt_token = tknzr.tokenize(tweet)
	# twt_token = word_tokenize(tweet)
	tweets_clean = []
	for word in twt_token:
		if (word not in stopwords_english and # remove stopwords
			word not in emoticons and # remove emoticons
			word not in string.punctuation): # remove punctuation
			#tweets_clean.append(word)
			stem_word = stemmer.stem(word) # stemming word
			# stem_word = word # no stemming
			tweets_clean.append(stem_word)

	return tweets_clean
	# return tweet


def read_and_gather_tweets(twt):
	""" input: tweets file name
		output: all the stemmed words in the tweets"""	
	allTokens = []	
	root='root/'
	f = open(root+twt)
	i=0
	for line in f:
		i = i+1
		if i <= 0:
			continue
		else:	
			print(i, end="\r")
			if i==4000:
				break
			tweetid = line.strip()
		# print(line)
			tweet = fetch_tweet(tweetid)
		# print(tweet)
		# os.system("pause")
			if tweet == None:
				continue
			else:
				clean_tokens = tokenize_and_clean_tweets(tweet)
				allTokens.append(clean_tokens)
				allTokens.append(tweet)
	return allTokens	


def co_occurance(tokens, number):
	""" input: tokens= token list
			   number= number of top frequent co-occuring bigrams to extract
		output: most frequent co-occuring bigrams"""
	finder = BigramCollocationFinder.from_words(tokens, window_size = 3)
	finder.apply_freq_filter(2)
	bigram_measures = nltk.collocations.BigramAssocMeasures()
	finder = finder.nbest(bigram_measures.pmi, number)
	# finder = finder.score_ngrams(bigram_measures.pmi)
	return finder	

def frequency(tokens, number):
	""" input: tokens= token list
			   number= number of top frequent words to extract
		output: most frequent tokens"""
	return nltk.FreqDist(tokens).most_common(number)

def frequent_names(tokens, names, number):
	""" inputs: tokens= tweets tokens
				names= named entities list
				number= how many frequent names to return
		output: list of occurances top number named entities"""
	occurances=[]
	for name in names:
	 	occurances.append([nltk.Text(tokens).count(name), name])

	occurances.sort(reverse=True)

	return occurances[:number]	




def recover_tweetfromtxt(file):
	f = open(file, encoding="utf-8")
	x = f.read()
	x = ast.literal_eval(x)

	return x

def txttocsv(root, outputFile):
	csvFile = open(outputFile, 'w', encoding='utf-8')
	csvWriter = csv.writer(csvFile)
	csvWriter.writerow(["text"]) 


	tweets=[]
	lis = os.listdir(root)
    
	for p in lis:
		tweets.extend(recover_tweetfromtxt(os.join(root,p)))
	for i in tweets:
		csvWriter.writerow([i])	

x = recover_tweetfromtxt("tokens/token.txt")                
print(frequency(x, 150))