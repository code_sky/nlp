#Recover tweets using tweet ids without Twitter API
#works only in Finalnd(change the start word if your IP is not in Finalnd)

import json
import os
from tqdm import tqdm
import nltk, re, pprint
from nltk import word_tokenize
from nltk.tokenize import TweetTokenizer
from nltk.corpus import stopwords 
from nltk.stem import PorterStemmer
from nltk.collocations import *
import string
import re
from urllib import request
import sys
from bs4 import BeautifulSoup


def fetch_tweet(tweetid):
	"""fetch tweet from id
		input: tweet id
		output: tweet text"""
	tweetid =str(tweetid)
	base = "https://twitter.com/anyuser/status/"
	url = base+tweetid
	try:
		html = request.urlopen(url).read().decode('utf8')
		raw = BeautifulSoup(html).get_text()

		start = 'Twitterissä: "'
		end = '"\n'
		st = raw.find(start)
		en = raw.find(end)

		if st == -1 or en == -1:
			return None
	
		tweet = raw[st+len(start):en]

		return tweet
	except:
		return None	

#print tweets from a file containing tweet ids
f = open('ids.txt')
for line in f:
	tweetid = line.strip()
	print(line)
	tweet = fetch_tweet(tweetid)
	print(tweet)
	os.system("pause")


