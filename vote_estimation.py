import pickle
import random
from statistics import mode

import nltk
from nltk.classify import ClassifierI
from nltk.classify.scikitlearn import SklearnClassifier
from nltk.tokenize import word_tokenize
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import BernoulliNB, MultinomialNB

import pandas as pd
import numpy as np
from tqdm import tqdm, tqdm_notebook

from vote_classifier import VoteClassifier

print("Loading classifiers and features")
classifiers = {
"NLTK Naive Bayes": "data/pickle/original_naive_bayes.pickle",
"Logistic Regression": "data/pickle/logistic_regression.pickle"
}

trained_classifiers = []
for classifier in classifiers.values():
    with open(classifier, "rb") as fc:
        trained_classifiers.append(pickle.load(fc))

voted_classifier = VoteClassifier(*trained_classifiers)

word_features5k_f = open("data/pickle/word_features5k.pickle", "rb")
word_features = pickle.load(word_features5k_f)
word_features5k_f.close()
print("-----features loaded------")

def find_features(document: str, features: list):
    """Tokenize tweet batches and extract sentiment features.
    inputs:document  -- tweet batch
            features  -- List of features
    output:feature list in tweet batches"""
    words = word_tokenize(document)
    _features = {w: (w in words) for w in features}
    return _features


def sentiment(text):
    """Get the sentiment from text.
    input: text -- Tweet text.
    outputs: sentiment (pos or neg)
            confidence """
    feats = find_features(text, word_features)
    return voted_classifier.sentiment(feats)


tqdm.pandas(tqdm)


for i in range(1, 2):
    print("Reading file", i)
    cs = pd.read_csv("csvbbc/"+ str(i) + ".csv")
    print("Total number of lines", len(cs.index))

    print("Detecting sentiment")
    cs["sentiment"] = cs["text"].progress_apply(sentiment)
    cs.to_csv("sentimentsbbc/"+ "tweets_with_sentiment_" + str(i) + ".csv")
    print("saved file %d to csv"%i)

tqdm.pandas(tqdm_notebook)

df = pd.DataFrame()
for i in range(1, 2):
    print("Reading index", i)
    d = pd.read_csv(open("sentimentsbbc/tweets_with_sentiment_" + str(i) + ".csv", 'r', encoding='utf-8'), encoding='utf-8',
                    engine='c', low_memory=False)
    df = pd.concat([d, df])
    print("File rows", len(d.index), "Total rows", len(df.index))
df.describe(include = 'all')
df.to_pickle('data/bbc.pickle')
print("all files with sentiments saved to pickle file")    