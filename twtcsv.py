#import tweet text from txt file to a csv file
import csv
import os
from optparse import OptionParser
import pandas as pd
from typing import List
import ast
import string
import nltk, re, pprint
from nltk import word_tokenize
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from bs4 import BeautifulSoup


stopwords_english = stopwords.words('english')
stemmer = PorterStemmer()
emoticons = set([
    ':-\)', ':\)', ';\)', ':o\)', ':\]', ':3', ':c\)', ':>', '=\]', '8\)', '=\)', ':\}',
    ':^\)', ':-D', ':D', '8-D', '8D', 'x-D', 'xD', 'X-D', 'XD', '=-D', '=D',
    '=-3', '=3', ':-\)\)', ":'-\)", ":'\)", '>:P', ':-P', ':P', 'X-P',
    'x-p', 'xp', 'XP', ':-p', ':p', '=p', ':-b', ':b', '>:\)', '>;\)', '>:-\)',
    '<3' , ':L', ':-/', '>:/', ':S', '>:\[', ':@', ':-\(', ':\[', ':-||', '=L', ':<',
    ':-\[', ':-<', '=/', '>:\(', ':\(', '>.<', ":'-\(", ":'\(", ':-c',
    ':c', ':\{', ';\(', '…', '“', '”'
    ])
emoji_pattern = re.compile("["
        u"\U0001F600-\U0001F64F"  # emoticons
        u"\U0001F300-\U0001F5FF"  # symbols & pictographs
        u"\U0001F680-\U0001F6FF"  # transport & map symbols
        u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                           "]+", flags=re.UNICODE)

def clean_tweets(tweet):
	"""clean tweet text
	input: text
	öutput:cleaned text"""

	# remove stock market tickers like $GE
	tweet = re.sub(r'\$\w*', '', tweet)
	# remove old style retweet text "RT"
	tweet = re.sub(r'^RT[\s]+', '', tweet)
	# remove hyperlinks
	tweet = re.sub(r'https?:\/\/.*[\r\n]*', '', tweet)
	# remove hashtags
	# only removing the hash # sign from the word
	tweet = re.sub(r'#', '', tweet)
	# remove @ sign
	# only removing the at @ sign from the word
	tweet = re.sub(r'@', '', tweet)
	# remove emojis
	tweet = emoji_pattern.sub(r'', tweet)
	# remove emoticons
	for i in emoticons:
		tweet = re.sub(i,'',tweet)

	return tweet

def recover_tweetfromtxt(file):
	"""recover tweets saved as list in text file
	input: file name
	output: list of tweets"""
	f = open(file, encoding="utf-8")
	x = f.read()
	x = ast.literal_eval(x)
	return x


def txttocsv(inroot, outroot):
	"""saving tweets to a csv file
	input: root folder where txt files are
	output: root file where csvs will be saved
	"""
	tweets=[]
	lis = os.listdir(inroot)
	j=1
	for p in lis:
		print(p)
		csvFile = open(outroot+"/"+"%d.csv"%j, 'w', encoding='utf-8')
		csvWriter = csv.writer(csvFile)
		csvWriter.writerow(["text"])
		# tweets.extend(recover_tweetfromtxt(root+"/"+p))
		tweets = recover_tweetfromtxt(inroot+"/"+p)
		j+=1
		twtbatch=''
		l=0
		for i in tweets:
			twtbatch+= i
			twtbatch = clean_tweets(twtbatch)
			if l%10 == 0:
				csvWriter.writerow([twtbatch])
				twtbatch=''
			l+=1				

txttocsv("root","csv")

