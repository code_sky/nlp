#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from newsapi import NewsApiClient
#get you own key fron BBC API
newsapi = NewsApiClient(api_key='2f7611782ab44c5700000000f775f97b9')

#fetch all the articles with " get_everything" 
all_articles = newsapi.get_everything(q='vote',
                                      sources='bbc-news,the-verge',
                                      domains='bbc.co.uk,techcrunch.com',
                                      from_param='2019-01-04',
                                      to='2015-01-04',
                                      language='en',
                                      sort_by='relevancy',
                                      page=1)

