
## Training

run `Train_classifier.py` to train sentiments from the corpus of movies

## Sentiment Analysis

Get the sentiment analysis of tweets using `vote_estimation.py` the result will be saved in a csv file in the folder sentimentbbc

### Vote support estimation

run `scorecalculation.py` to estimate the vote support, note that you have to manually edit it to switch between bbc and twitter both the keywords and dataset 
run `nlp_ui.py` to use the UI

### Miscellaneous 

`recoverTweets.py` Recover tweets using tweet ids without Twitter API, works only in Finalnd(change the start word if your IP is not in Finalnd)
`misc.py`contain scripts and function to get various informations about the data such as frequency of words, use from misc.py import frequency for example
bbcfiles folder contain scripts used to gather articles from BBC API

## references

* https://github.com/aalind0/NLP-Sentiment-Analysis-Twitter](https://github.com/aalind0/NLP-Sentiment-Analysis-Twitter
* https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/PDI7IN
